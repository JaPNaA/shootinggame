import Game from "./game/game.js";
import Canvas from "./engine/canvas/canvas.js";
import Loader from "./engine/loader/loader.js";
import Resources from "./engine/loader/resources.js";

const canvas = new Canvas(innerWidth * devicePixelRatio, innerHeight * devicePixelRatio);
const loader = new Loader();

loader.load()
    .then(resources => start(resources))
    .catch(err => showFatalError(err));

document.body.removeChild(document.getElementById("fallback") as Node);
canvas.appendTo(document.body);

function start(resources: Resources) {
    const game = new Game(canvas, resources);
    console.log(game);
}

addEventListener("resize", () => canvas.resize(innerWidth * devicePixelRatio, innerHeight * devicePixelRatio));

function showFatalError(err: any) {
    console.error(err);

    let message = "Unknown Error";
    let stack = new Error().stack;

    if (err) {
        if (err instanceof Error) {
            message = err.message;
            stack = err.stack || stack;
        } else {
            message = JSON.stringify(err);
        }
    }

    const div = document.createElement("div");
    div.classList.add("fatal-error");

    {
        const title = document.createElement("div");
        title.classList.add("title");
        title.innerText = "A fatal error occured";
        div.appendChild(title);
    } {
        const divBody = document.createElement("div");
        divBody.classList.add("body");
        divBody.innerText = message + "\n" + stack;
        div.appendChild(divBody);
    }

    document.body.appendChild(div);
}
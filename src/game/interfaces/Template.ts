import GameHooks from "../gameHooks.js";

export default interface Template<T> {
    generate(gHooks: GameHooks): T;
}

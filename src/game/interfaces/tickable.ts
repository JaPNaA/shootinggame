interface Tickable {
    tick(deltaTime: number): void;
}

export default Tickable;
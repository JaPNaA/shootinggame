import Wieldable from "../items/Wieldable.js";
import Vec2 from "../../engine/types/vec.js";

interface Wielder {
    pos: Vec2;
    centerOffset: Vec2;

    wieldItem(wieldable: Wieldable): void;
}

export default Wielder;
import Item from "./Item.js";
import Wielder from "../interfaces/wielder.js";

abstract class Wieldable extends Item {
    protected player?: Wielder;

    constructor() {
        super();
    }

    public abstract playerAction(): void;
    public abstract tick(deltaTime: number): void;

    public unbindFromPlayer() {
        this.player = undefined;
    }

    public bindToPlayer(player: Wielder) {
        this.player = player;
    }
}

export default Wieldable;
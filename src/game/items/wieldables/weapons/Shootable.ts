import Weapon from "../Weapon.js";
import Vec2 from "../../../../engine/types/vec.js";
import Mouse from "../../../../engine/ui/mouse/mouse.js";
import Template from "../../../interfaces/Template.js";
import GameHooks from "../../../gameHooks.js";
import Projectile from "../../../objs/types/projectile.js";

class Shootable extends Weapon implements Template<Shootable> {
    protected cooldownTime: number;

    protected warmth: number = 0;

    constructor(private options: ShootableOptions) {
        super();
        this.cooldownTime = options.cooldownTime;
    }

    public generate(gHooks: GameHooks): Shootable {
        const x = new Shootable(this.options);
        x._setGHooks(gHooks);
        x.texture = gHooks.R.getTexture(this.options.textureId);
        return x;
    }

    public tick(deltaTime: number) {
        if (this.isWarm()) {
            this.warmth -= deltaTime;
        }
    }

    public playerAction(): void {
        while (this.isCool()) {
            this.warmth += this.cooldownTime;
            this.shoot();
        }
    }

    protected isWarm(): boolean {
        return this.warmth > 0;
    }

    protected isCool(): boolean {
        return this.warmth <= 0;
    }

    protected shoot(): void {
        this.shootTowardsMouse(this.options.projectile);
    }

    protected getPlayerPos(): Vec2 {
        if (!this.player) { throw new Error("Cannot get player position when not equipped"); }
        return Vec2.add(this.player.pos, this.player.centerOffset);
    }

    protected getMouseDirFrom(playerPos: Vec2): Vec2 {
        return Vec2.subtract(Vec2.add(Mouse.pos, this.gHooks.camera.getPos()), playerPos);
    }

    protected shootTowardsMouse(projectileType: Template<Projectile>) {
        const playerPos = this.getPlayerPos();
        const directions = this.options.directions || 1;
        const mouseDirFromPlayer = this.getMouseDirFrom(playerPos);

        if (directions > 1) {
            const pointingDirection = mouseDirFromPlayer.getAngle();
            const pointingDistance = mouseDirFromPlayer.getLength();
            const spread = this.options.spread || 0;

            for (let i = 0; i < directions; i++) {
                const direction = Vec2.fromPolar((i - directions * 0.5) / directions * spread + pointingDirection, pointingDistance);
                const newBall = projectileType.generate(this.gHooks);
                newBall.setPosAndTarget(playerPos, direction);
                this.gHooks.addObj(newBall);
            }
        } else {
            const newBall = projectileType.generate(this.gHooks);
            newBall.setPosAndTarget(playerPos, mouseDirFromPlayer);
            this.gHooks.addObj(newBall);
        }
    }
}

interface ShootableOptions {
    element: string;
    type: string;
    cooldownTime: number;
    textureId: string;
    projectile: Template<Projectile>;
    directions?: number;
    spread?: number;
}

export default Shootable;
import Wieldable from "../Wieldable.js";

abstract class Weapon extends Wieldable {
    constructor() {
        super();
    }
}

export default Weapon;
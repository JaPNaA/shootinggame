import GameHooks from "../gameHooks.js";
import Texture from "../../engine/canvas/types/texture.js";

abstract class Item {
    protected gHooks!: GameHooks;
    public texture!: Texture;

    constructor() { }

    public _setGHooks(gHooks: GameHooks) {
        this.gHooks = gHooks;
    }
}

export default Item;
import Color from "../../engine/canvas/types/color.js";
import Dim from "../../engine/canvas/types/dim.js";
import Vec2 from "../../engine/types/vec.js";
import Light from "../objs/projectiles/Light.js";
import Ball from "../objs/projectiles/Ball.js";
import Shootable from "./wieldables/weapons/Shootable.js";
import Blink from "../objs/projectiles/Blink.js";

const projectiles = {
    airBall: new Ball({
        damage: 4,
        dim: new Dim(0, 0, 12, 18),
        texture: 'objs.projectiles.wands.airBall'
    }),
    blink: new Blink({
        damage: 1,
        dim: new Dim(0, 0, 12, 18),
        texture: 'objs.projectiles.wands.airBall',
        timeAlive: 4
    }),
    laserBeam: new Light({
        damage: 1,
        dim: new Dim(0, 0, 1, 1),
        originVec: new Vec2(0, 0),
        color: new Color(255, 0, 0)
    }),
    sunBeam: new Light({
        damage: 1,
        dim: new Dim(0, 0, 1, 1),
        originVec: new Vec2(0, 0),
        color: new Color(255, 255, 0)
    })
};

export const itemsRegistry = {
    "laser": new Shootable({
        type: "laser",
        element: "light",
        cooldownTime: 0.2,
        projectile: projectiles.laserBeam,
        textureId: 'items.wieldable.weapons.shootable.laser.laser'
    }),
    "sunLaser": new Shootable({
        type: "laser",
        element: "light",
        cooldownTime: 0.2,
        projectile: projectiles.sunBeam,
        textureId: 'items.wieldable.weapons.shootable.laser.laser',
        directions: 8,
        spread: Math.PI * 2
    }),
    "airWand": new Shootable({
        type: "wand",
        element: "air",
        cooldownTime: 1,
        projectile: projectiles.airBall,
        textureId: 'items.wieldable.weapons.shootable.wands.air.airWand'
    }),
    "gustWand": new Shootable({
        type: "wand",
        element: "air",
        cooldownTime: 0.4,
        projectile: projectiles.airBall,
        textureId: 'items.wieldable.weapons.shootable.wands.air.gustWand'
    }),
    "stormWand": new Shootable({
        type: "wand",
        element: "air",
        cooldownTime: 0.25,
        projectile: projectiles.airBall,
        textureId: 'items.wieldable.weapons.shootable.wands.air.stormWand'
    }),
    "solarWindWand": new Shootable({
        type: "wand",
        element: "fire",
        cooldownTime: 0.15,
        projectile: projectiles.airBall, // TODO: FireBall
        textureId: 'items.wieldable.weapons.shootable.wands.fire.solarWindWand'
    }),
    "staff": new Shootable({
        type: "staff",
        element: "mana",
        cooldownTime: 1,
        projectile: projectiles.blink,
        textureId: 'default'
    }),
    "handheldStaff": new Shootable({
        type: "staff",
        element: "mana",
        cooldownTime: 0.1,
        projectile: projectiles.blink,
        textureId: 'default'
    })
};

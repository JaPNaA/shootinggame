import Canvas from "../engine/canvas/canvas.js";
import UiMan from "../engine/ui/uiMan.js";
import World from "./world.js";
import Obj from "./objs/types/obj.js";
import Resources from "../engine/loader/resources.js";
import Camera from "../engine/utils/camera.js";

class GameHooks {
    public X: Canvas;
    public R: Resources;
    public uiMan: UiMan;
    public world: World;
    public camera: Camera;

    constructor(X: Canvas, R: Resources, uiMan: UiMan, world: World, camera: Camera) {
        this.X = X;
        this.R = R;
        this.uiMan = uiMan;
        this.world = world;
        this.camera = camera;
    }

    public addObj(obj: Obj) {
        this.world.add(obj);
    }
}

export default GameHooks;
import Drawable from "../../engine/canvas/interfaces/drawable.js";
import Entity from "../objs/types/entity.js";
import Dim from "../../engine/canvas/types/dim.js";
import Color from "../../engine/canvas/types/color.js";
import RenderingX from "../../engine/canvas/renderingX.js";

class HealthBar implements Drawable {
    private static bgColor: Color = new Color(232, 34, 34);
    private static fgColor: Color = new Color(38, 229, 32);

    private entity: Entity;

    private dimBg: Dim;
    private dimFg: Dim;

    private width: number;

    constructor(entity: Entity) {
        this.entity = entity;

        const x = 0;
        const y = entity.dim.height + 8;
        const width = entity.dim.width;
        const height = 6;

        this.dimBg = new Dim(x, y, width, height);
        this.dimFg = new Dim(x, y, 0, height);

        this.width = width;
    }

    public update() {
        const val = this.entity.health / this.entity.maxHealth;
        this.dimFg.width = this.width * val;
    }

    public draw(X: RenderingX) {
        X.rect(this.dimBg, HealthBar.bgColor);
        X.rect(this.dimFg, HealthBar.fgColor);
    }
}

export default HealthBar;
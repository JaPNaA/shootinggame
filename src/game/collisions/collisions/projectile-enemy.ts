import Projectile from "../../objs/types/projectile.js";
import EnemyTypeEntity from "../../objs/entities/types/enemy.js";
import isIntersectingLines from "../utils/isIntersectingLines.js";
import objExists from "../../objs/utils/objExists.js";
import isCollidingAABB from "../utils/isCollidingAABB.js";

export default function projectile_enemy_collideFunc(projectile: Projectile, enemy: EnemyTypeEntity) {
    if (!objExists(projectile) || !objExists(enemy)) return;

    const enemyXLeft = enemy.dim.x + enemy.pos.x;
    const enemyXRight = enemyXLeft + enemy.dim.width;
    const enemyYTop = enemy.dim.y + enemy.pos.y;
    const enemyYBottom = enemyYTop + enemy.dim.height;

    const lastProjectileX = projectile.pos.x + projectile.centerOffset.x;
    const lastProjectileY = projectile.pos.y + projectile.centerOffset.y;
    const projectileX = projectile.lastPos.x + projectile.centerOffset.x;
    const projectileY = projectile.lastPos.y + projectile.centerOffset.y;

    // checks projectile aabb boxes and
    // checks projectile line with X through enemy box
    if (isCollidingAABB(enemy, projectile) || isIntersectingLines(
        enemyXLeft, enemyYTop, enemyXRight, enemyYBottom,
        lastProjectileX, lastProjectileY, projectileX, projectileY
    ) || isIntersectingLines(
        enemyXRight, enemyYTop, enemyXLeft, enemyYBottom,
        lastProjectileX, lastProjectileY, projectileX, projectileY
    )) {
        projectile.onHitEnemy(enemy);
        enemy.hitByBullet(projectile);
    }
}
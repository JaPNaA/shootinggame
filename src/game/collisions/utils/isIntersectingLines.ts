export default function isIntersectingLines(
    ax1: number, ay1: number, ax2: number, ay2: number,
    bx1: number, by1: number, bx2: number, by2: number
) {
    var t, u, v;
    t = (ax2 - ax1) * (by2 - by1) - (bx2 - bx1) * (ay2 - ay1);
    if (t === 0) return false;
    v = ((by2 - by1) * (bx2 - ax1) + (bx1 - bx2) * (by2 - ay1)) / t;
    u = ((ay1 - ay2) * (bx2 - ax1) + (ax2 - ax1) * (by2 - ay1)) / t;
    return 0 < v && v < 1 && 0 < u && u < 1;
};
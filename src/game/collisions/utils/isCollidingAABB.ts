import Dim from "../../../engine/canvas/types/dim.js";
import Vec2 from "../../../engine/types/vec.js";

interface Rect {
    pos: Vec2;
    dim: Dim;
}

export default function isCollidingAABB(a: Rect, b: Rect) {
    const aX = a.dim.x + a.pos.x;
    const aY = a.dim.y + a.pos.y;
    const aW = a.dim.width;
    const aH = a.dim.height;

    const bX = b.dim.x + b.pos.x;
    const bY = b.dim.y + b.pos.y;
    const bW = b.dim.width;
    const bH = b.dim.height;

    return (
        aX < bX + bW &&
        aX + aW > bX &&
        aY < bY + bH &&
        aY + aH > bY
    );
}
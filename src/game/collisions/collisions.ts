import projectile_enemy_collideFunc from "./collisions/projectile-enemy.js";

export default {
    projectile_enemy: projectile_enemy_collideFunc
};
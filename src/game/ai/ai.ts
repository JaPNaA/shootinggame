import Entity from "../objs/types/entity.js";
import GameHooks from "../gameHooks.js";

abstract class Ai {
    protected self: Entity;
    protected gHooks!: GameHooks;

    constructor(self: Entity) {
        this.self = self;
    }

    public _setGHooks(gHooks: GameHooks) {
        this.gHooks = gHooks;
    }

    abstract tick(): void;
}

export default Ai;
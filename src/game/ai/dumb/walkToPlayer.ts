import Ai from "../ai.js";
import PlayerTypeEntity from "../../objs/entities/types/player.js";
import Entity from "../../objs/types/entity.js";
import objExists from "../../objs/utils/objExists.js";
import Vec2 from "../../../engine/types/vec.js";

class WalkToPlayer extends Ai {
    private target?: PlayerTypeEntity;

    constructor(self: Entity) {
        super(self);
    }

    public tick(): void {
        if (objExists(this.target)) {
            this.walkTowardsTarget();
        } else {
            this.findTarget();

            if (objExists(this.target)) {
                this.walkTowardsTarget();
            }
        }
    }

    private findTarget(): void {
        for (let obj of this.gHooks.world.objs) {
            if (obj instanceof PlayerTypeEntity) {
                this.target = obj;
            }
        }
    }

    private walkTowardsTarget(): void {
        const angle = Vec2.angle(this.self.pos, (this.target as PlayerTypeEntity).pos);
        this.self.pos.translatePolar(angle, 1);
    }
}

export default WalkToPlayer;
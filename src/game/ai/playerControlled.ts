import Ai from "./ai.js";
import Entity from "../objs/types/entity.js";
import Vec2 from "../../engine/types/vec.js";
import Keyboard from "../../engine/ui/keyboard/keyboard.js";

class PlayerControlled extends Ai {
    public movingLeft: boolean;
    public movingRight: boolean;
    public movingUp: boolean;
    public movingDown: boolean;

    constructor(self: Entity) {
        super(self);

        this.movingLeft = false;
        this.movingRight = false;
        this.movingUp = false;
        this.movingDown = false;
    }

    public tick() {
        this.updateMovingKeys();
    }

    private updateMovingKeys(): void {
        // w | up
        this.movingUp = Keyboard.keys[87] || Keyboard.keys[38];
        // a | left
        this.movingLeft = Keyboard.keys[65] || Keyboard.keys[37];
        // s | down
        this.movingDown = Keyboard.keys[83] || Keyboard.keys[40];
        // d | right
        this.movingRight = Keyboard.keys[68] || Keyboard.keys[39];
    }

    public getMovingDir(): Vec2 {
        let x = this.getMovingX();
        let y = this.getMovingY();

        if (x && y) {
            x *= Math.SQRT1_2;
            y *= Math.SQRT1_2;
        }

        return new Vec2(x, y);
    }

    public getMovingX(): number {
        let x = 0;

        if (this.movingLeft) {
            x--;
        }
        if (this.movingRight) {
            x++;
        }

        return x;
    }

    public getMovingY(): number {
        let y = 0;

        if (this.movingUp) {
            y--;
        }
        if (this.movingDown) {
            y++;
        }

        return y;
    }

    private startMoveLeft(): void {
        this.movingLeft = true;
    }

    private startMoveRight(): void {
        this.movingRight = true;
    }

    private startMoveUp(): void {
        this.movingUp = true;
    }

    private startMoveDown(): void {
        this.movingDown = true;
    }


    private stopMoveLeft(): void {
        this.movingLeft = false;
    }

    private stopMoveRight(): void {
        this.movingRight = false;
    }

    private stopMoveUp(): void {
        this.movingUp = false;
    }

    private stopMoveDown(): void {
        this.movingDown = false;
    }
}

export default PlayerControlled;
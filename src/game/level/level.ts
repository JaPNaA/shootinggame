import GameHooks from "../gameHooks.js";
import Vec2 from "../../engine/types/vec.js";
import ItemDropFactory from "../objs/items/factory.js";
import Template from "../interfaces/Template.js";
import Item from "../items/Item.js";
import Obj from "../objs/types/obj.js";

abstract class Level {
    protected gHooks: GameHooks;

    constructor(gHooks: GameHooks) {
        this.gHooks = gHooks;
    }

    protected createItemAt(itemTemplate: Template<Item>, pos: Vec2): void {
        const item = ItemDropFactory.fromItem(this.gHooks, itemTemplate.generate(this.gHooks));
        item.teleportTo(pos);
        this.gHooks.world.add(item);
    }

    protected createObjAt(objTemplate: Template<Obj>, pos: Vec2): void {
        const obj = objTemplate.generate(this.gHooks);
        obj.teleportTo(pos);
        this.gHooks.world.add(obj);
    }

    public abstract setup(): void;
}

export default Level;
import Level from "../level.js";
import Player from "../../objs/entities/player/player.js";
import Dummy from "../../objs/entities/enemy/dummy.js";
import Vec2 from "../../../engine/types/vec.js";
import Zombie from "../../objs/entities/enemy/zombie.js";
import { itemsRegistry } from "../../items/itemsRegistry.js";
import BackgroundTiling from "../../objs/BackgroundTiling.js";
import Dim from "../../../engine/canvas/types/dim.js";

class LevelN1Test extends Level {
    public setup(): void {
        this.gHooks.world.add(new BackgroundTiling({
            dim: new Dim(0, 0, 2000, 2000),
            pos: new Vec2(0, 0),
            tileSize: new Vec2(32, 32),
            texture: 'tiles.tatami'
        }).generate(this.gHooks));

        const player = new Player();
        player.teleportTo(new Vec2(800, 500));
        player._setGHooks(this.gHooks);
        player.wieldItem(itemsRegistry.handheldStaff.generate(this.gHooks));

        this.gHooks.world.add(player);
        this.gHooks.camera.follow(player.pos);
        this.gHooks.camera.addOffset(player.centerOffset);

        this.createObjAt(new Dummy(), new Vec2(256, 360));
        this.createObjAt(new Zombie(), new Vec2(512, 360));

        this.createItemAt(itemsRegistry.solarWindWand, new Vec2(512 + 64 * 1, 256));

        this.createItemAt(itemsRegistry.stormWand, new Vec2(512, 256 + 64 * 0));
        this.createItemAt(itemsRegistry.gustWand, new Vec2(512, 256 + 64 * 1));
        this.createItemAt(itemsRegistry.airWand, new Vec2(512, 256 + 64 * 2));

        this.createItemAt(itemsRegistry.laser, new Vec2(512 + 64 * 2, 256 + 64 * 0));
        this.createItemAt(itemsRegistry.sunLaser, new Vec2(512 + 64 * 2, 256 + 64 * 1));

        this.createItemAt(itemsRegistry.staff, new Vec2(512 + 64 * 3, 256 + 64 * 0));
        this.createItemAt(itemsRegistry.handheldStaff, new Vec2(512 + 64 * 3, 256 + 64 * 1));
    }
}

export default LevelN1Test;
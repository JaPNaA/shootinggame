import Canvas from "../engine/canvas/canvas.js";
import Color from "../engine/canvas/types/color.js";
import World from "./world.js";
import GameHooks from "./gameHooks.js";
import UiMan from "../engine/ui/uiMan.js";
import Resources from "../engine/loader/resources.js";
import LevelN1Test from "./level/levels/levelN1_test.js";
import Camera from "../engine/utils/camera.js";
import Vec2 from "../engine/types/vec.js";

class Game {
    private X: Canvas;
    private R: Resources;

    private world: World;
    private uiMan: UiMan;
    private camera: Camera;

    private hooks: GameHooks;
    private reqanfHandle?: number;

    constructor(X: Canvas, R: Resources) {
        this.X = X;
        this.R = R;

        this.world = new World();
        this.uiMan = new UiMan();
        this.camera = new Camera();

        this.hooks = new GameHooks(this.X, this.R, this.uiMan, this.world, this.camera);

        this.setup();
        this.drawLoop();
    }

    private setup() {
        const level = new LevelN1Test(this.hooks);
        level.setup();
    }

    private drawLoop() {
        this.reqanfHandle = requestAnimationFrame(this.draw.bind(this));
    }

    private draw() {
        this.tick();

        this.X.background(new Color(0, 0, 0));

        this.camera.setOffset(new Vec2(-this.X.width / 2, -this.X.height / 2));

        this.X.translateCamera(this.camera);
        this.world.drawAll(this.X);
        this.X.untranslateCamera(this.camera);

        this.drawLoop();
    }

    private tick() {
        this.world.tickAll();
        this.world.checkCollisions();
        this.world.removeMarkedObjs();

        this.camera.tick();
    }
}

export default Game;
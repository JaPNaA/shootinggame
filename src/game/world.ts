import Obj from "./objs/types/obj.js";
import RenderingX from "../engine/canvas/renderingX.js";
import Projectile from "./objs/types/projectile.js";
import EnemyTypeEntity from "./objs/entities/types/enemy.js";
import collisions from "./collisions/collisions.js";

class World {
    public now: number;
    public objs: Obj[] = [];

    constructor() {
        this.now = performance.now();
    }

    public add(obj: Obj) {
        this.objs.push(obj);
    }

    public drawAll(X: RenderingX) {
        for (let obj of this.objs) {
            obj.draw(X);
        }
    }

    public tickAll() {
        const now = performance.now();
        const dt = (now - this.now) / 1000;
        this.now = now;

        for (let obj of this.objs) {
            obj.tick(dt);
        }
    }

    public removeMarkedObjs() {
        for (let i = 0; i < this.objs.length; i++) {
            const obj = this.objs[i];

            if (obj.toBeRemoved) {
                this.objs.splice(i, 1);
            }
        }
    }

    public checkCollisions() {
        for (let obj of this.objs) {
            if (obj instanceof Projectile) {
                this.projectileCollide(obj);
            }
        }
    }

    private projectileCollide(projectile: Projectile) {
        for (let obj of this.objs) {
            if (obj instanceof EnemyTypeEntity) {
                collisions.projectile_enemy(projectile, obj);
            }
        }
    }
}

export default World;
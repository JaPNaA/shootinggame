import Canvas from "../../engine/canvas/canvas.js";
import RenderingX from "../../engine/canvas/renderingX.js";
import Dim from "../../engine/canvas/types/dim.js";
import Texture from "../../engine/canvas/types/texture.js";
import Vec2 from "../../engine/types/vec.js";
import GameHooks from "../gameHooks.js";
import Template from "../interfaces/Template.js";
import Obj from "./types/obj.js";

export default class BackgroundTiling extends Obj implements Template<BackgroundTiling> {
    public dim: Dim;
    private baseTexture!: Texture;
    private texture!: Texture;

    constructor(private options: BackgroundTilingOptions) {
        super();
        this.dim = options.dim;
        this.pos = options.pos;
    }

    generate(gHooks: GameHooks): BackgroundTiling {
        const x = new BackgroundTiling(this.options);
        x._setGHooks(gHooks);
        return x;
    }

    public _setGHooks(gHooks: GameHooks): this {
        this.baseTexture = gHooks.R.getTexture(this.options.texture);
        this.generateTexture();
        return super._setGHooks(gHooks);
    }

    public generateTexture() {
        const canvas = new Canvas(this.dim.width, this.dim.height);
        const dim = new Dim(0, 0, this.options.tileSize.x, this.options.tileSize.y);

        for (let y = 0; y < this.dim.height; y += this.options.tileSize.y) {
            for (let x = 0; x < this.dim.width; x += this.options.tileSize.x) {
                dim.x = x + this.dim.x;
                dim.y = y + this.dim.y;
                canvas.rectTex(dim, this.baseTexture);
            }
        }

        this.texture = canvas.asTexture();
    }

    protected drawRelative(X: RenderingX): void {
        X.rectTex(this.dim, this.texture);
    }

    protected tickAfter(deltaTime: number): void { return; }
}

interface BackgroundTilingOptions {
    dim: Dim;
    pos: Vec2;
    tileSize: Vec2;
    texture: string;
}
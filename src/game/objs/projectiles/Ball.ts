import Projectile from "../types/projectile.js";
import Dim from "../../../engine/canvas/types/dim.js";
import Template from "../../interfaces/Template.js";
import RenderingX from "../../../engine/canvas/renderingX.js";
import GameHooks from "../../gameHooks.js";
import randomWithNegative from "../utils/randomWithNegative.js";
import Texture from "../../../engine/canvas/types/texture.js";
import Vec2 from "../../../engine/types/vec.js";

class Ball extends Projectile implements Template<Ball> {
    protected static speed = 1000;
    protected static maxDist = 1000;
    protected static damage = 1;

    public dim: Dim;

    public damage: number = Ball.damage;

    private texture!: Texture;

    constructor(private options: BallOptions) {
        super();
        this.dim = options.dim;
        this.damage = options.damage;
    }

    public generate(gHooks: GameHooks): Ball {
        const airBall = new Ball(this.options);
        airBall._setGHooks(gHooks);
        airBall.texture = gHooks.R.getTexture(this.options.texture);
        return airBall;
    }

    public setPosAndTarget(pos: Vec2, target: Vec2): void {
        super.setPosAndTarget(pos, target);
        this.velocity.scale(Ball.speed);
    }

    protected tickAfter(): void {
        this.distTraveled += this.adjToTime(Ball.speed);

        if (this.distTraveled > Ball.maxDist) {
            this.toBeRemoved = true;
        }

        return super.tickAfter();
    }

    protected drawRelative(X: RenderingX) {
        X.translateVec(this.centerOffset);
        X.rotate(this.ang + Math.PI / 2);
        X.untranslateVec(this.centerOffset);
        X.translate(randomWithNegative() * 4, randomWithNegative() * 4);

        X.rectTex(this.dim, this.texture);
    }
}

interface BallOptions {
    dim: Dim;
    damage: number;
    texture: string;
}

export default Ball;
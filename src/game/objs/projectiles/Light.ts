import RenderingX from "../../../engine/canvas/renderingX.js";
import Color from "../../../engine/canvas/types/color.js";
import Dim from "../../../engine/canvas/types/dim.js";
import Vec2 from "../../../engine/types/vec.js";
import GameHooks from "../../gameHooks.js";
import Template from "../../interfaces/Template.js";
import Projectile from "../types/projectile.js";

class Light extends Projectile implements Template<Light> {
    protected static speed = 29979.2458; // can't go higher: reduced accuracy of line
    protected static maxDist = Light.speed;

    public dim: Dim;
    public damage: number;

    private tickedOnce: boolean = false;

    constructor(private options: LightOptions) {
        super();
        this.dim = options.dim;
        this.damage = options.damage;
        this.setup();
    }

    public generate(gHooks: GameHooks): Light {
        const light = new Light(this.options);
        light._setGHooks(gHooks);
        return light;
    }

    public setPosAndTarget(pos: Vec2, velocity: Vec2): void {
        super.setPosAndTarget(pos, velocity);
        this.velocity.scale(Light.speed);
    }

    protected setup(): void {
        super.setup();
        this.velocity.scale(Light.speed);
    }

    protected tickAfter(): void {
        if (this.tickedOnce) {
            this.toBeRemoved = true;
        }
        this.tickedOnce = true;

        this.pos.translate(this.velocity);
    }

    public onHitEnemy() { }

    protected drawRelative(X: RenderingX): void {
        X.line(this.options.originVec, Vec2.invert(this.velocity), 2, this.options.color);
    }
}

interface LightOptions {
    color: Color;
    damage: number;
    dim: Dim;
    originVec: Vec2;
}

export default Light;
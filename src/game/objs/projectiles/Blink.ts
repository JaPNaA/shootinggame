import Projectile from "../types/projectile.js";
import Dim from "../../../engine/canvas/types/dim.js";
import Template from "../../interfaces/Template.js";
import RenderingX from "../../../engine/canvas/renderingX.js";
import GameHooks from "../../gameHooks.js";
import randomWithNegative from "../utils/randomWithNegative.js";
import Texture from "../../../engine/canvas/types/texture.js";
import Vec2 from "../../../engine/types/vec.js";

class Blink extends Projectile implements Template<Blink> {
    protected static damage = 1;

    public dim: Dim;

    public damage: number = Blink.damage;

    private texture!: Texture;

    constructor(private options: BlinkOptions) {
        super();
        this.dim = options.dim;
        this.damage = options.damage;
    }

    public generate(gHooks: GameHooks): Blink {
        const blink = new Blink(this.options);
        blink._setGHooks(gHooks);
        blink.texture = gHooks.R.getTexture(this.options.texture);
        return blink;
    }

    public setPosAndTarget(pos: Vec2, target: Vec2): void {
        super.setPosAndTarget(Vec2.add(target, pos), target);
        this.velocity.scale(0);
    }

    protected tickAfter(): void {
        this.distTraveled += this.adjToTime(1);

        if (this.distTraveled > this.options.timeAlive) {
            this.toBeRemoved = true;
        }

        return super.tickAfter();
    }

    protected drawRelative(X: RenderingX) {
        X.translateVec(this.centerOffset);
        X.rotate(this.ang + Math.PI / 2);
        X.untranslateVec(this.centerOffset);
        X.translate(randomWithNegative() * 4, randomWithNegative() * 4);

        X.rectTex(this.dim, this.texture);
    }
}

interface BlinkOptions {
    dim: Dim;
    damage: number;
    texture: string;
    timeAlive: number;
}

export default Blink;
import Drawable from "../../../engine/canvas/interfaces/drawable.js";
import RenderingX from "../../../engine/canvas/renderingX.js";
import Tickable from "../../interfaces/tickable.js";
import Vec2 from "../../../engine/types/vec.js";
import Dim from "../../../engine/canvas/types/dim.js";
import GameHooks from "../../gameHooks.js";

abstract class Obj implements Drawable, Tickable {
    public abstract dim: Dim;
    public pos: Vec2;
    public toBeRemoved = false;

    /** deltaTime in seconds */
    protected deltaTime: number = 0;
    protected gHooks!: GameHooks;

    constructor() {
        this.pos = new Vec2(0, 0);
    }

    public _setGHooks(gHooks: GameHooks): this {
        this.gHooks = gHooks;
        return this;
    }

    protected setup() { }

    public draw(X: RenderingX) {
        const transformState = X.getTransformState();

        this.drawFixed(X);

        X.translateVec(this.pos);
        this.drawRelative(X);
        X.setTransformState(transformState);
    }

    public tick(deltaTime: number) {
        this.tickBefore(this.deltaTime);

        if (!this.shouldTickAfter()) { return; }
        this.deltaTime = deltaTime;
        this.tickAfter(deltaTime);
    }

    public teleportTo(pos: Vec2) {
        this.pos = pos;
    }

    protected adjToTime(x: number) {
        return x * this.deltaTime;
    }

    protected adjVecToTime(vec: Vec2) {
        return Vec2.scale(vec, this.deltaTime);
    }

    protected abstract drawRelative(X: RenderingX): void;
    protected abstract tickAfter(deltaTime: number): void;
    protected drawFixed(X: RenderingX): void { };
    protected tickBefore(lastDeltaTime: number): void { };

    protected shouldTickAfter(): boolean {
        return !this.toBeRemoved;
    }
}

export default Obj;
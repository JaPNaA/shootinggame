import Obj from "./obj.js";
import Vec2 from "../../../engine/types/vec.js";
import EnemyTypeEntity from "../entities/types/enemy.js";
import RenderingX from "../../../engine/canvas/renderingX.js";

abstract class Projectile extends Obj {
    public abstract damage: number;
    public centerOffset: Vec2;
    public lastPos: Vec2;

    protected velocity: Vec2;

    protected distTraveled: number = 0;
    protected ang: number = 0;

    protected target?: Vec2;

    constructor() {
        super();

        this.lastPos = new Vec2(0, 0);
        this.velocity = new Vec2(0, 0);
        this.centerOffset = new Vec2(0, 0);
    }

    /**
     * @param pos Position of projectile
     * @param target velocity = normalized(target)
     */
    public setPosAndTarget(pos: Vec2, target: Vec2) {
        this.pos = pos.clone();
        this.lastPos = pos.clone();
        this.target = target;

        const targetWithOffset = Vec2.subtract(this.target, this.centerOffset);

        this.velocity = Vec2.fromAngle(targetWithOffset.getAngle());
        this.ang = this.velocity.getAngle();
    }

    protected setup() {
        super.setup();
        this.centerOffset.x = this.dim.x + this.dim.width / 2;
        this.centerOffset.y = this.dim.y + this.dim.height / 2;
    }

    protected tickBefore() {
        this.lastPos.x = this.pos.x;
        this.lastPos.y = this.pos.y;
    }

    protected tickAfter() {
        this.translateBasedOnVelocity();
    }

    protected translateBasedOnVelocity() {
        this.pos.translate(
            this.adjVecToTime(
                this.velocity
            )
        );
    }

    public onHitEnemy(enemy: EnemyTypeEntity): void {
        this.toBeRemoved = true;
    }

    protected abstract drawRelative(X: RenderingX): void;
}

export default Projectile;
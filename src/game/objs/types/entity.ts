import Obj from "./obj.js";
import Projectile from "./projectile.js";
import HealthBar from "../../ui/healthBar.js";
import RenderingX from "../../../engine/canvas/renderingX.js";

abstract class Entity extends Obj {

    public abstract maxHealth: number;
    public abstract health: number;

    private healthBar!: HealthBar;

    constructor() {
        super();
    }

    protected setup() {
        super.setup();
        this.healthBar = new HealthBar(this);
    }

    protected drawRelative(X: RenderingX): void {
        this.healthBar.update();
        this.healthBar.draw(X);
    }

    public hitByBullet(projectile: Projectile): void {
        this.health -= projectile.damage;
        if (this.health <= 0) {
            this.toBeRemoved = true;
        }
    }
}

export default Entity;
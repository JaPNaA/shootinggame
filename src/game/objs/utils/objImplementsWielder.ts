import Wielder from "../../interfaces/wielder.js";

export default function objImplementsWielder(obj: any): obj is Wielder {
    return obj && typeof obj.wieldItem === "function";
}
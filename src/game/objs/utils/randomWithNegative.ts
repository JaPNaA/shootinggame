export default function randomWithNegative(): number {
    return Math.random() * 2 - 1;
}
import Obj from "../types/obj.js";

export default function objExists(obj: Obj | undefined | null): boolean {
    return Boolean(obj && !obj.toBeRemoved);
}
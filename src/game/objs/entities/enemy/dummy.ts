import EnemyTypeEntity from "../types/enemy.js";
import RenderingX from "../../../../engine/canvas/renderingX.js";
import Dim from "../../../../engine/canvas/types/dim.js";
import GameHooks from "../../../gameHooks.js";
import Texture from "../../../../engine/canvas/types/texture.js";
import Template from "../../../interfaces/Template.js";

class Dummy extends EnemyTypeEntity implements Template<Dummy> {
    public maxHealth = 1000;
    public health = this.maxHealth;
    public isTemplate = true;

    public dim: Dim = new Dim(0, 0, 40, 56);
    private texture!: Texture;

    constructor() {
        super();

        this.setup();
    }

    public generate(gHooks: GameHooks): Dummy {
        const dummy = new Dummy()._setGHooks(gHooks);
        dummy.texture = gHooks.R.getTexture('objs.entities.enemy.dummy');
        return dummy;
    }

    protected drawRelative(X: RenderingX) {
        super.drawRelative(X);
        X.rectTex(this.dim, this.texture);
    }

    protected tickAfter() { }
}

export default Dummy;
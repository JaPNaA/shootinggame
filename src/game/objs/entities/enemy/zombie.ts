import EnemyTypeEntity from "../types/enemy.js";
import Dim from "../../../../engine/canvas/types/dim.js";
import Color from "../../../../engine/canvas/types/color.js";
import RenderingX from "../../../../engine/canvas/renderingX.js";
import WalkToPlayer from "../../../ai/dumb/walkToPlayer.js";
import GameHooks from "../../../gameHooks.js";
import Template from "../../../interfaces/Template.js";

class Zombie extends EnemyTypeEntity implements Template<Zombie> {
    public maxHealth = 10;
    public health = this.maxHealth;

    public dim: Dim = new Dim(0, 0, 32, 32);
    private color: Color = new Color(89, 181, 30);

    private ai: WalkToPlayer;

    constructor() {
        super();

        this.ai = new WalkToPlayer(this);
        this.setup();
    }

    public generate(gHooks: GameHooks): Zombie {
        const zombie = new Zombie();
        zombie._setGHooks(gHooks);
        return zombie;
    }

    public _setGHooks(gHooks: GameHooks) {
        this.ai._setGHooks(gHooks);
        return super._setGHooks(gHooks);
    }

    protected drawRelative(X: RenderingX) {
        super.drawRelative(X);
        X.rect(this.dim, this.color);
    }

    protected tickAfter() {
        this.ai.tick();
    }
}

export default Zombie;
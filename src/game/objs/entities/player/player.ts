import Dim from "../../../../engine/canvas/types/dim.js";
import RenderingX from "../../../../engine/canvas/renderingX.js";
import GameHooks from "../../../gameHooks.js";
import PlayerTypeEntity from "../types/player.js";
import Vec2 from "../../../../engine/types/vec.js";
import Mouse from "../../../../engine/ui/mouse/mouse.js";
import Wieldable from "../../../items/Wieldable.js";
import Texture from "../../../../engine/canvas/types/texture.js";
import EaseAnimation from "../../../../engine/utils/easeAnimation.js";
import { easeSin } from "../../../../engine/utils/easingFuncs.js";
import Wielder from "../../../interfaces/wielder.js";

class Player extends PlayerTypeEntity implements Wielder {
    private static speed: number = 128;
    private static bobbingIntensity: number = 4;
    private static bobbingInterval: number = Math.PI;

    private bobbingAnimation: EaseAnimation;

    public health = 100;
    public maxHealth = 100;

    public dim: Dim;

    private texture!: Texture;
    private wielding?: Wieldable;

    private facingRight: boolean;

    constructor() {
        super();

        this.bobbingAnimation = new EaseAnimation(easeSin, {
            length: Player.bobbingInterval,
            intensity: Player.bobbingIntensity,
            repeat: true
        });

        this.dim = new Dim(0, 0, 42, 42);

        this.facingRight = false;

        this.setup();
    }

    public _setGHooks(gHooks: GameHooks): this {
        this.texture = gHooks.R.getTexture('objs.entities.player.player');
        return super._setGHooks(gHooks);
    }

    public wieldItem(wieldable: Wieldable) {
        this.wielding = wieldable;
        wieldable.bindToPlayer(this);
    }

    protected tickAfter(deltaTime: number): void {
        this.pos.translate(
            Vec2.scale(
                this.adjVecToTime(this.ai.getMovingDir()),
                Player.speed
            )
        );

        if (Mouse.leftDown) {
            this.useWielding();
        }

        // TODO: Make better, 
        // allow cursor to change facing direction,
        // add assets for facing up/down
        if (this.ai.movingLeft) {
            this.facingRight = false;
        } else if (this.ai.movingRight) {
            this.facingRight = true;
        }

        this.tickAll(deltaTime);
    }

    private tickAll(deltaTime: number) {
        if (this.wielding) {
            this.wielding.tick(deltaTime);
        }
    }

    private useWielding() {
        if (!this.wielding) { return; }
        this.wielding.playerAction();
    }

    protected drawRelative(X: RenderingX): void {
        super.drawRelative(X);


        this.applyBobbingAnimation(X);
        this.flipIfRequired(X);
        X.rectTex(this.dim, this.texture);
    }

    private applyBobbingAnimation(X: RenderingX) {
        this.bobbingAnimation.stepBy(this.deltaTime);
        X.translateY(this.bobbingAnimation.get());
    }

    private flipIfRequired(X: RenderingX) {
        if (this.facingRight) {
            X.scaleX(-1);
            X.translateX(-this.dim.width);
        }
    }
}

export default Player;
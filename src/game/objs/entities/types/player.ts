import Entity from "../../types/entity.js";
import GameHooks from "../../../gameHooks.js";
import Vec2 from "../../../../engine/types/vec.js";
import PlayerControlled from "../../../ai/playerControlled.js";

abstract class PlayerTypeEntity extends Entity {
    public centerOffset: Vec2;
    protected ai: PlayerControlled;

    constructor() {
        super();

        this.ai = new PlayerControlled(this);
        this.centerOffset = new Vec2(0, 0);
    }

    public _setGHooks(gHooks: GameHooks): this {
        this.ai._setGHooks(gHooks);
        return super._setGHooks(gHooks);
    }

    protected setup() {
        super.setup();

        this.centerOffset.x = this.dim.x + this.dim.width / 2;
        this.centerOffset.y = this.dim.y + this.dim.height / 2;
    }

    public tick(deltaTime: number) {
        this.ai.tick();
        return super.tick(deltaTime);
    }
}

export default PlayerTypeEntity;
import Item from "../../items/Item.js";
import Obj from "../types/obj.js";
import Dim from "../../../engine/canvas/types/dim.js";
import RenderingX from "../../../engine/canvas/renderingX.js";
import Texture from "../../../engine/canvas/types/texture.js";

abstract class ItemDrop extends Obj {
    protected abstract item: Item;

    protected texture: Texture = null as any as Texture;
    public dim: Dim;

    constructor() {
        super();

        this.dim = new Dim(0, 0, 0, 0);
    }

    protected setup() {
        this.dim.width = this.item.texture.width * 2;
        this.dim.height = this.item.texture.height * 2;

        this.texture = this.item.texture;
        super.setup();
    }

    protected drawRelative(X: RenderingX) {
        X.rectTex(this.dim, this.texture);
    }

    protected tickAfter() { }
}

export default ItemDrop;
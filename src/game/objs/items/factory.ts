import ItemDrop from "./itemDrop.js";
import Wieldable from "../../items/Wieldable.js";
import WieldableDrop from "./wieldableDrop.js";
import Item from "../../items/Item.js";
import GameHooks from "../../gameHooks.js";

class ItemDropFactory {
    public static fromItem(gHooks: GameHooks, item: Wieldable): WieldableDrop;
    public static fromItem(gHooks: GameHooks, item: Item): ItemDrop;

    public static fromItem(gHooks: GameHooks, item: Item): ItemDrop {
        if (item instanceof Wieldable) {
            return new WieldableDrop(item)._setGHooks(gHooks);
        } else {
            throw new Error("Factory cannot convert this type: not implemented");
        }
    }
}

export default ItemDropFactory;
import ItemDrop from "./itemDrop.js";
import objImplementsWielder from "../utils/objImplementsWielder.js";
import isCollidingAABB from "../../collisions/utils/isCollidingAABB.js";
import Wieldable from "../../items/Wieldable.js";

class WieldableDrop extends ItemDrop {
    protected item: Wieldable;

    constructor(item: Wieldable) {
        super();

        this.item = item;
        this.setup();
    }

    tickAfter() {
        for (let obj of this.gHooks.world.objs) {
            if (objImplementsWielder(obj) && isCollidingAABB(obj, this)) {
                obj.wieldItem(this.item);
            }
        }
    }
}

export default WieldableDrop;
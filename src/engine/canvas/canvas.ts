import RenderingX from "./renderingX.js";

class Canvas extends RenderingX {
    private elm: HTMLCanvasElement;

    constructor(width: number, height: number) {
        const elm = document.createElement("canvas");
        elm.width = width;
        elm.height = height;

        super(elm);

        this.elm = elm;
    }

    public appendTo(elm: HTMLElement) {
        elm.appendChild(this.elm);
    }

    public resize(width: number, height: number) {
        this.elm.width = width;
        this.elm.height = height;
        super.resize(width, height);
    }
}

export default Canvas;
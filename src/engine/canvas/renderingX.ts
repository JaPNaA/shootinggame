import Color from "./types/color.js";
import Dim from "./types/dim.js";
import Vec2 from "../types/vec.js";
import Texture from "./types/texture.js";
import Camera from "../utils/camera.js";

class RenderingX {
    protected X: CanvasRenderingContext2D;
    public width = 0;
    public height = 0;

    constructor(private canvas: HTMLCanvasElement) {
        const X = canvas.getContext("2d");
        if (!X) { throw new Error("Cannot initalize 2d canvas"); }
        this.X = X;

        this.width = canvas.width;
        this.height = canvas.height;

        this.X.imageSmoothingEnabled = false;
        this.X.imageSmoothingQuality = "low";
    }

    public resize(width: number, height: number) {
        this.width = width;
        this.height = height;

        this.X.imageSmoothingEnabled = false;
        this.X.imageSmoothingQuality = "low";
    }

    public asTexture(): Texture {
        return new Texture(this.canvas, 0, 0, this.width, this.height);
    }

    // Drawing
    // ----------------------------------------------------------------------------------------
    public background(color: Color): void {
        color.applyToFill(this);
        this.X.fillRect(0, 0, this.width, this.height);
    }

    public rect(dim: Dim, color: Color): void {
        color.applyToFill(this);
        this.X.fillRect(dim.x, dim.y, dim.width, dim.height);
    }

    public rectTex(dim: Dim, texture: Texture): void {
        this.X.drawImage(
            texture.imageSrc, texture.x, texture.y, texture.width, texture.height,
            dim.x, dim.y, dim.width, dim.height
        );
    }

    public line(from: Vec2, to: Vec2, lineWidth: number, color: Color): void {
        color.applyToStroke(this);
        this.X.lineWidth = lineWidth;

        this.X.beginPath();
        this.X.moveTo(from.x, from.y);
        this.X.lineTo(to.x, to.y);
        this.X.stroke();
    }

    public _setFillColor(str: string): void {
        this.X.fillStyle = str;
    }

    public _setStrokeColor(str: string): void {
        this.X.strokeStyle = str;
    }

    public _rect(x: number, y: number, width: number, height: number): void {
        this.X.fillRect(x, y, width, height);
    }

    // Transformations
    // ----------------------------------------------------------------------------------------
    public getTransformState(): DOMMatrix {
        return this.X.getTransform();
    }

    public setTransformState(matrix: DOMMatrix): void {
        this.X.setTransform(matrix);
    }

    public translate(x: number, y: number): void {
        this.X.translate(x, y);
    }

    public translateX(x: number) {
        this.X.translate(x, 0);
    }

    public translateY(y: number) {
        this.X.translate(0, y);
    }

    public untranslate(x: number, y: number): void {
        this.X.translate(-x, -y);
    }

    public translateVec(vec: Vec2): void {
        this.X.translate(vec.x, vec.y);
    }

    public untranslateVec(vec: Vec2): void {
        this.X.translate(-vec.x, -vec.y);
    }

    public translateCamera(camera: Camera): void {
        this.untranslateVec(camera.getPos());
    }

    public untranslateCamera(camera: Camera): void {
        this.translateVec(camera.getPos());
    }

    public rotate(ang: number): void {
        this.X.rotate(ang);
    }

    public scale(mag: number): void {
        this.X.scale(mag, mag);
    }

    public scaleX(x: number): void {
        this.X.scale(x, 1);
    }

    public scaleY(y: number): void {
        this.X.scale(1, y);
    }
}

export default RenderingX;
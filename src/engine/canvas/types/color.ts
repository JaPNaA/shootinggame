import RenderingX from "../renderingX.js";

class Color {
    r: number;
    g: number;
    b: number;
    a: number;

    constructor(r: number, g: number, b: number, a?: number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a || 1;
    }

    public applyToFill(X: RenderingX) {
        X._setFillColor(this.toString());
    }

    public applyToStroke(X: RenderingX) {
        X._setStrokeColor(this.toString());
    }

    public toString() {
        return `rgba(${this.r},${this.g},${this.b},${this.a})`;
    }
}

export default Color;
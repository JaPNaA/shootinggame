import RenderingX from "../renderingX.js";

interface Drawable {
    draw(X: RenderingX): void;
}

export default Drawable;
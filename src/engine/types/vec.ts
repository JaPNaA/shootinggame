class Vec2 {
    x: number;
    y: number;

    static fromAngle(ang: number): Vec2 {
        return new Vec2(Math.cos(ang), Math.sin(ang));
    }

    static fromPolar(ang: number, length: number): Vec2 {
        return new Vec2(Math.cos(ang) * length, Math.sin(ang) * length);
    }

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }


    public static invert(vec: Vec2): Vec2 {
        return new Vec2(-vec.x, -vec.y);
    }

    public static dot(a: Vec2, b: Vec2): Vec2 {
        return new Vec2(
            a.x * b.x,
            a.y * b.y
        );
    }

    public static add(a: Vec2, b: Vec2): Vec2 {
        return new Vec2(
            a.x + b.x,
            a.y + b.y
        );
    }

    public static subtract(a: Vec2, b: Vec2): Vec2 {
        return new Vec2(
            a.x - b.x,
            a.y - b.y
        );
    }

    public static scale(vec: Vec2, scalar: number): Vec2 {
        return new Vec2(
            vec.x * scalar,
            vec.y * scalar
        );
    }

    public static angle(from: Vec2, to: Vec2): number {
        return this.subtract(to, from).getAngle();
    }


    public translate(vec: Vec2): void {
        this.x += vec.x;
        this.y += vec.y;
    }

    public translatePolar(angle: number, dist: number) {
        this.x += Math.cos(angle) * dist;
        this.y += Math.sin(angle) * dist;
    }

    public scale(scalar: number): void {
        this.x *= scalar;
        this.y *= scalar;
    }

    public lerp(amount: number, target: Vec2): void {
        this.x += (target.x - this.x) * amount;
        this.y += (target.y - this.y) * amount;
    }

    public copy(target: Vec2): void {
        this.x = target.x;
        this.y = target.y;
    }

    public getAngle(): number {
        return Math.atan2(this.y, this.x);
    }

    public getLength(): number {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public clone(): Vec2 {
        return new Vec2(this.x, this.y);
    }
}

export default Vec2;
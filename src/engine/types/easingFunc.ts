type EasingFunc = (progress: number) => number;

export default EasingFunc;
import Loader from "./loader.js";
import Texture from "../canvas/types/texture.js";

const ASSET_PATH = "../static/assets/";
const IMG_PATH = ASSET_PATH + "img/";

class Resources {
    private static imgs = [
        "placeholder.png",
        "objs/entities/player/player.png",
        "objs/entities/enemy/dummy.png",
        "objs/projectiles/wands/airBall.png",
        "items/wieldable/weapons/shootable/wands/air/airWand.png",
        "items/wieldable/weapons/shootable/wands/air/gustWand.png",
        "items/wieldable/weapons/shootable/wands/air/stormWand.png",
        "items/wieldable/weapons/shootable/wands/fire/solarWindWand.png",
        "items/wieldable/weapons/shootable/laser/laser.png",
        "tiles/tatami.png"
    ];

    private textures: { [x: string]: Texture } = {};

    private loader: Loader;

    constructor(loader: Loader) {
        this.loader = loader;
    }

    public getTexture(name: string) {
        if (this.textures.hasOwnProperty(name)) {
            return this.textures[name];
        } else {
            console.warn("Resource " + name + " was requested but did not exist.\nA placeholder was returned instead");
            return this.textures["placeholder"];
        }
    }

    public load() {
        this.loadImgs();
    }

    private loadImgs() {
        for (let path of Resources.imgs) {
            this.loader._loadImage(IMG_PATH + path)
                .then(img => this.addImage(path, img));
        }
    }

    private addImage(path: string, img: HTMLImageElement) {
        this.textures[this.getKeyFromPath(path)] = new Texture(img, 0, 0, img.width, img.height);
    }

    private getKeyFromPath(path: string): string {
        const withoutExtension = 
            path.slice(0, path.lastIndexOf('.'));
        return withoutExtension.replace(/\//g, '.')
    }
}

export default Resources;
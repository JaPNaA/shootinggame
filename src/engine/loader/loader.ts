import Resources from "./resources.js";

class Loader {
    public resources: Resources;
    private allPromises: Promise<any>[];

    constructor() {
        this.resources = new Resources(this);
        this.allPromises = [];
    }

    public async load(): Promise<Resources> {
        this.resources.load();
        await Promise.all(this.allPromises);
        return this.resources;
    }

    public _loadImage(path: string): Promise<HTMLImageElement> {
        const promise = new Promise<HTMLImageElement>(function (acc, rej) {
            const img = document.createElement("img");
            img.src = path;
            img.addEventListener("load", function () {
                acc(img);
            });
            img.addEventListener("error", function () {
                rej("Failed to load asset " + img.src);
            });
        });

        this.allPromises.push(promise);

        return promise;
    }
}

export default Loader;
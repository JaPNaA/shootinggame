import Vec2 from "../types/vec.js";
import Tickable from "../../game/interfaces/tickable.js";

class Camera implements Tickable {
    private pos: Vec2;
    private following?: Vec2;
    private offset: Vec2;

    constructor(startingPos?: Vec2) {
        this.pos = startingPos || new Vec2(0, 0);
        this.offset = new Vec2(0, 0);
    }

    public tick() {
        if (!this.following) return;
        this.pos = Vec2.add(this.following, this.offset);
    }

    public getPos(): Vec2 {
        return this.pos;
    }

    public follow(vec: Vec2): void {
        this.following = vec;
    }

    public setOffset(vec: Vec2): void {
        this.offset = vec;
    }

    public addOffset(vec: Vec2): void {
        this.offset = Vec2.add(this.offset, vec);
    }
}

export default Camera;
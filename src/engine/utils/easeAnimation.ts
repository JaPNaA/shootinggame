import EasingFunc from "../types/easingFunc.js";

class EaseAnimation {
    private easingFunc: EasingFunc;
    private step: number;

    private length: number;
    private intensity: number;
    private animationRepeats: boolean;

    constructor(easingFunc: EasingFunc, options: {
        length?: number,
        intensity?: number,
        repeat?: boolean
    }) {
        this.easingFunc = easingFunc;
        this.step = 0;
        this.length = options.length || 1;
        this.animationRepeats = options.repeat || false;
        this.intensity = options.intensity || 1;
    }

    public get(): number {
        return this.easingFunc(this.step / this.length) * this.intensity;
    }

    public stepBy(i: number): void {
        this.step += i;
        this.wrapAhead();
    }

    public stepBack(i: number): void {
        this.step -= i;
        this.wrapBack();
    }

    public setStep(i: number): void {
        this.step = i;
        this.wrap();
    }

    private wrap(): boolean {
        return this.wrapAhead() || this.wrapBack();
    }

    private wrapAhead(): boolean {
        if (this.step > this.length) {
            if (this.animationRepeats) {
                this.step %= this.length;
            } else {
                this.step = this.length;
            }

            return true;
        }

        return false;
    }

    private wrapBack(): boolean {
        if (this.step < 0) {
            if (this.animationRepeats) {
                this.step %= this.length;
                this.step += this.length;
            } else {
                this.step = 0;
            }

            return true;
        }

        return false;
    }
}

export default EaseAnimation;
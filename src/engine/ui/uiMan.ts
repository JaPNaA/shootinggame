import Keyboard from "./keyboard/keyboard.js";
import Mouse from "./mouse/mouse.js";

class UiMan {
    public keyboard: Keyboard;
    public mouse: Mouse;
    
    constructor() {
        this.keyboard = new Keyboard();
        this.mouse = new Mouse();
    }
}

export default UiMan;
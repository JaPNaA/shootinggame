import KeyboardEvt from "./keyboardEvt.js";

type Cb = (event: KeyboardEvt) => void;

const MAX_KEYCODE = 255;

class Keyboard {
    public static keys: boolean[];

    private keydownListeners: Cb[] = [];
    private keyupListeners: Cb[] = [];

    constructor() {
        addEventListener("keydown", this.handleKeydown.bind(this));
        addEventListener("keyup", this.handleKeyup.bind(this));
        addEventListener("blur", this.clearKeys.bind(this));
    }

    public onKeydown(func: Cb): void {
        this.keydownListeners.push(func);
    }

    public onKeyup(func: Cb): void {
        this.keyupListeners.push(func);
    }

    private handleKeydown(event: KeyboardEvent) {
        Keyboard.keys[event.keyCode] = true;
        this.dispatchKeydown(KeyboardEvt.fromEvent(event));
    }
    private handleKeyup(event: KeyboardEvent) {
        Keyboard.keys[event.keyCode] = false;
        this.dispatchKeyup(KeyboardEvt.fromEvent(event));
    }

    private dispatchKeydown(event: KeyboardEvt) {
        this.dispatchArr(this.keydownListeners, event);
    }

    private dispatchKeyup(event: KeyboardEvt) {
        this.dispatchArr(this.keyupListeners, event);
    }

    private dispatchArr(arr: Cb[], event: KeyboardEvt) {
        for (let cb of arr) {
            cb(event);
        }
    }

    private clearKeys() {
        for (let i = 0; i <= MAX_KEYCODE; i++) {
            if (Keyboard.keys[i]) {
                this.dispatchKeyup(new KeyboardEvt(i));
                Keyboard.keys[i] = false;
            }
        }
    }
}

Keyboard.keys = [];

for (let i = 0; i <= MAX_KEYCODE; i++) {
    Keyboard.keys[i] = false;
}

export default Keyboard;
class KeyboardEvt {
    keyCode: number;
    
    constructor(keyCode: number) {
        this.keyCode = keyCode;
    }

    static fromEvent(event: KeyboardEvent): KeyboardEvt {
        return new KeyboardEvt(event.keyCode);
    }
}

export default KeyboardEvt;
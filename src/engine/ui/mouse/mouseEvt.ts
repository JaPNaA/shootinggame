import Vec2 from "../../types/vec.js";

class MouseEvt {
    public button: number;
    public pos: Vec2;

    constructor(button: number, pos: Vec2) {
        this.button = button;
        this.pos = pos.clone();
    }

    static fromEvent(event: MouseEvent) {
        return new MouseEvt(
            event.button,
            new Vec2(event.x, event.y)
        );
    }
}

export default MouseEvt;
import MouseEvt from "./mouseEvt.js";
import MouseButton from "./mouseButtonEnums.js";
import Vec2 from "../../types/vec.js";

type Cb = (event: MouseEvt) => void;

class Mouse {
    public static pos: Vec2;
    public static leftDown: boolean = false;
    public static middleDown: boolean = false;
    public static rightDown: boolean = false;

    private mouseMoveListeners: Cb[] = [];
    private mouseDownListeners: Cb[] = [];
    private mouseUpListeners: Cb[] = [];

    constructor() {
        addEventListener("mousedown", this.handleMouseDown.bind(this));
        addEventListener("mouseup", this.handleMouseUp.bind(this));
        addEventListener("mousemove", this.handleMouseMove.bind(this));
        addEventListener("blur", this.clearButtons.bind(this));
        addEventListener("contextmenu", this.preventDefault.bind(this));
    }

    public onMouseDown(cb: Cb) {
        this.mouseDownListeners.push(cb);
    }

    public onMouseUp(cb: Cb) {
        this.mouseUpListeners.push(cb);
    }

    public onMouseMove(cb: Cb) {
        this.mouseMoveListeners.push(cb);
    }

    private handleMouseDown(e: MouseEvent) {
        this.dispatchMouseDown(MouseEvt.fromEvent(e));
        this.setButtonState(e.button, true);
    }

    private handleMouseUp(e: MouseEvent) {
        this.dispatchMouseUp(MouseEvt.fromEvent(e));
        this.setButtonState(e.button, false);
    }

    private handleMouseMove(e: MouseEvent) {
        this.dispatchMouseMove(MouseEvt.fromEvent(e));
        this.updateMousePosition(e.x, e.y);
    }


    private dispatchMouseDown(e: MouseEvt) {
        this.dispatchArr(this.mouseDownListeners, e);
    }

    private dispatchMouseUp(e: MouseEvt) {
        this.dispatchArr(this.mouseUpListeners, e);
    }

    private dispatchMouseMove(e: MouseEvt) {
        this.dispatchArr(this.mouseMoveListeners, e);
    }


    private dispatchArr(arr: Cb[], event: MouseEvt) {
        for (let cb of arr) {
            cb(event);
        }
    }

    private setButtonState(button: number, state: boolean) {
        if (button === MouseButton.left) {
            Mouse.leftDown = state;
        } else if (button === MouseButton.middle) {
            Mouse.middleDown = state;
        } else if (button === MouseButton.right) {
            Mouse.rightDown = state;
        }
    }

    private updateMousePosition(x: number, y: number) {
        Mouse.pos.x = x;
        Mouse.pos.y = y;
    }

    private clearButtons() {
        if (Mouse.leftDown) {
            this.dispatchMouseUp(new MouseEvt(MouseButton.left, Mouse.pos))
            Mouse.leftDown = false;
        }

        if (Mouse.rightDown) {
            this.dispatchMouseUp(new MouseEvt(MouseButton.right, Mouse.pos));
            Mouse.rightDown = false;
        }

        if (Mouse.middleDown) {
            this.dispatchMouseUp(new MouseEvt(MouseButton.middle, Mouse.pos));
            Mouse.middleDown = false;
        }
    }

    private preventDefault(e: Event) {
        e.preventDefault();
    }
}

Mouse.pos = new Vec2(0, 0);

export default Mouse;
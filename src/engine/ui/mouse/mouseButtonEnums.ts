enum MouseButtonEnums {
    left = 0,
    middle = 1,
    right = 2
}

export default MouseButtonEnums;